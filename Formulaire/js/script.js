$(function() {
    $(window).scroll(function() {
        let scroll = $(window).scrollTop();
        if (scroll >= 300) {
            $(".navbar").addClass('smaller');
        } else {
            $(".navbar").removeClass("smaller");
        }
    });

    $(window).resize(function () {
        let width = $(window).width();
        if (width < 780) {
            $('[data-toggle="buttons"]').removeClass('btn-group').addClass('btn-group-vertical');
        } else {
            $('[data-toggle="buttons"]').addClass('btn-group').removeClass('btn-group-vertical');
        }
    });
});


  let $hamburger = $(".hamburger");
  $hamburger.on("click", function(e) {
    $hamburger.toggleClass("is-active");
      });
